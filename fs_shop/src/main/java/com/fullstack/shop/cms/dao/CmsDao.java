package com.fullstack.shop.cms.dao;

import org.apache.ibatis.annotations.Mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.fullstack.shop.cms.entity.Cms;

/**
 * 
 * @author Administrator
 *
 */
@Mapper
public interface CmsDao extends BaseMapper<Cms> {
	
}