package com.fullstack.shop.cms.web;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.fullstack.common.exceptions.BusinessException;
import com.fullstack.common.web.RequestUtils;
import com.fullstack.common.web.ServiceController;
import com.fullstack.shop.cms.entity.Cms;

@RestController  
@RequestMapping("/cms")  
public class CmsController extends ServiceController {
	
	/**
	 * 列表
	 * @param request
	 * @return
	 * @throws BusinessException
	 */
	@RequestMapping("list")
    public JSONObject list(HttpServletRequest request,Cms cms) throws BusinessException {
		EntityWrapper<Cms> e = this.entityInit(cms);
        return this.retResult(cmsService.selectList(e));
    }
	
	@RequestMapping("groupByType")
    public JSONObject groupByType(HttpServletRequest request,Cms cms) throws BusinessException {
		EntityWrapper<Cms> e = this.entityInit(cms);
		e.groupBy("type");
        return this.retResult(cmsService.selectList(e));
    }
	/**
	 * 新增
	 * @param request
	 * @param Cms
	 * @return
	 * @throws BusinessException
	 */
	@RequestMapping("create")
    public JSONObject createCms(HttpServletRequest request,Cms cms) throws BusinessException {
		cmsService.create(cms);
        return this.retResult(cms);
    }
	/**
	 * 修改
	 * @param request
	 * @param Cms
	 * @return
	 * @throws BusinessException
	 */
	@RequestMapping("update")
    public JSONObject updateCms(HttpServletRequest request,Cms cms) throws BusinessException {
		cmsService.editById(cms);
        return this.retResult(cms);
    }
	/**
	 * 删除
	 * @param request
	 * @param Cms
	 * @return
	 * @throws BusinessException
	 */
	@RequestMapping("del")
    public JSONObject delCms(HttpServletRequest request,Cms cms) throws BusinessException {
		cmsService.delById(cms);
        return this.retResult(success_del);
    }
	
	/**
	 * 批量删除
	 * @param request
	 * @param Cms
	 * @return
	 * @throws BusinessException
	 */
	@RequestMapping("batchDel")
    public JSONObject batchDel(HttpServletRequest request,Cms cms) throws BusinessException {
		String[] ids = RequestUtils.getStrings(request,"ids[]");
		if(ids!=null && ids.length>0){
			cmsService.batchDel(cms,ids);
	        return this.retResult(success_del);
		}else{
			return this.retResult("删除数据不存在");
		}
    }
	
}
